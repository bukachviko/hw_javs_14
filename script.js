const themeSwitchers = document.querySelectorAll(".change__theme");

themeSwitchers.forEach(switcher => {
    switcher.addEventListener('click', function () {
        document.querySelectorAll('.change__theme').forEach((element) => {
            if (element.dataset.theme !== this.dataset.theme) {
                element.style.display = 'block'
            } else {
                element.style.display = 'none'
            }
        })

        console.log(this.dataset.theme)

        applyTheme(this.dataset.theme);

        localStorage.setItem('theme', this.dataset.theme);

    });
});

function applyTheme(themeName) {
    let themeUrl = `./styles/theme.${themeName}.css`;

    document.querySelector('[title="theme"]').setAttribute('href', themeUrl);
}

let activeTheme = localStorage.getItem('theme');
if (activeTheme === null) {
    applyTheme('color');
} else {
    applyTheme(activeTheme);
}